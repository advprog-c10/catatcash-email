package com.c10.catatcashemail.controller;

import com.c10.catatcashemail.dto.EmailDto;
import com.c10.catatcashemail.service.EmailService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = EmailController.class)
@ExtendWith(MockitoExtension.class)
class EmailControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private EmailService emailService;

    private EmailDto emailDto;

    private ObjectMapper objectMapper;

    @BeforeEach
    void init() {
        emailDto = new EmailDto();
        emailDto.setEmail("advprogc10@gmail.com");
        objectMapper = new ObjectMapper();
    }

    @Test
    void testPostNotifyRegistrationShouldBeOk() throws Exception {
        this.mvc.perform(post("/registerEmail")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(emailDto)))
                .andDo(print())
                .andExpect(status().isOk());
        verify(emailService, times(1)).sendRegistrationEmail(emailDto);
    }

    @Test
    void testPostNotifyRegistrationShouldBeBadRequest() throws Exception {
        emailDto.setEmail("hahahaha");
        this.mvc.perform(post("/registerEmail")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(emailDto)))
                .andDo(print())
                .andExpect(status().isBadRequest());
        verify(emailService, times(1)).sendRegistrationEmail(emailDto);
    }
}
