package com.c10.catatcashemail.service;

import com.c10.catatcashemail.dto.EmailDto;
import nl.altindag.log.LogCaptor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;

@ExtendWith(MockitoExtension.class)
class EmailServiceImplTest {

    @Mock
    private JavaMailSender javaMailSender;

    @InjectMocks
    private EmailServiceImpl emailService;

    private EmailDto emailDto;

    private String sending;

    private String sent;

    private String fail;

    private LogCaptor logCaptor;

    @BeforeEach
    void init() {
        emailDto = new EmailDto();
        emailDto.setEmail("advprogc10@gmail.com");
        logCaptor = LogCaptor.forClass(EmailServiceImpl.class);
    }

    @Test
    void testServiceSendRegistrationEmailSuccess() {
        sending = String.format("Sending email to %s...", emailDto.getEmail());
        sent = String.format("Email sent to %s!", emailDto.getEmail());

        emailService.sendRegistrationEmail(emailDto);

        assertThat(logCaptor.getLogs())
                .hasSize(2)
                .containsExactly(sending, sent);
    }

    @Test
    void testServiceSendRegistrationEmailFail() {
        emailDto.setEmail("hahahaha");

        sending = String.format("Sending email to %s...", emailDto.getEmail());
        fail = String.format("Failed to send email to %s...", emailDto.getEmail());

        doThrow(new MailSendException("Invalid Email")).when(javaMailSender).send((SimpleMailMessage) any());

        emailService.sendRegistrationEmail(emailDto);

        assertThat(logCaptor.getLogs())
                .hasSize(2)
                .containsExactly(sending, fail);
    }
}
