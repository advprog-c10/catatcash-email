package com.c10.catatcashemail.service;

import com.c10.catatcashemail.dto.EmailDto;

public interface EmailService {
    void sendRegistrationEmail(EmailDto emailDto);
}
