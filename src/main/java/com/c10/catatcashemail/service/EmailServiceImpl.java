package com.c10.catatcashemail.service;

import com.c10.catatcashemail.dto.EmailDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class EmailServiceImpl implements EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Async
    @Override
    public void sendRegistrationEmail(EmailDto emailDto) {
        log.info(String.format("Sending email to %s...", emailDto.getEmail()));

        try {
            var mail = new SimpleMailMessage();
            mail.setTo(emailDto.getEmail());
            mail.setFrom("catatcash@gmail.com");
            mail.setSubject("Welcome to CatatCash!");
            mail.setText("Thank you for choosing us as your finance managing web application. This is an application designed to help you manage your finances, keep track of your target budgets, and to check your transaction report. We are looking forward to make your financial life better!");
            javaMailSender.send(mail);
            log.info(String.format("Email sent to %s!", emailDto.getEmail()));
        } catch (Exception e) {
            log.error(String.format("Failed to send email to %s...", emailDto.getEmail()), e);
        }
    }
}
