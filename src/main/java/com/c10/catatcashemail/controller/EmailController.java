package com.c10.catatcashemail.controller;

import com.c10.catatcashemail.dto.EmailDto;
import com.c10.catatcashemail.service.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.regex.Pattern;

@RestController
@Slf4j
public class EmailController {

    @Autowired
    private EmailService emailService;

    @PostMapping(path = "/registerEmail", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<EmailDto> notifyRegistration(@RequestBody EmailDto emailDto) {
        var pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        emailService.sendRegistrationEmail(emailDto);
        return pattern.matcher(emailDto.getEmail()).matches() ?
                ResponseEntity.ok(emailDto) : ResponseEntity.badRequest().build();
    }
}
