package com.c10.catatcashemail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class CatatcashEmailApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatatcashEmailApplication.class, args);
	}

}
