# CatatCash Email
## Final Project Advanced Programming 2020/2021 of Group C10

[![pipeline status](https://gitlab.com/advprog-c10/catatcash-email/badges/master/pipeline.svg)](https://gitlab.com/advprog-c10/catatcash-email/-/commits/master)
[![coverage report](https://gitlab.com/advprog-c10/catatcash-email/badges/master/coverage.svg)](https://gitlab.com/advprog-c10/catatcash-email/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=advprog-c10_catatcash-email&metric=alert_status)](https://sonarcloud.io/dashboard?id=advprog-c10_catatcash-email)

### Description
CatatCash microservice to handle email requests.

### Link
- Main Service: https://gitlab.com/advprog-c10/catatcash
- Email Service: https://gitlab.com/advprog-c10/catatcash-email
